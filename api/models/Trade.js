/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    TradeID: {
      type: 'number'
    },
    Timestamp: {
      type: 'number'
    },
    Market: {
      type: 'string'
    },
    Amount: {
      type: 'number'
    },
    Unit: {
      type: 'string'
    },
    Price: {
      type: 'number'
    },
    Currency: {
      type: 'string'
    },
    Total: {
      type: 'number'
    },
    BuyOrderID: {
      type: 'number'
    },
    SellOrderID: {
      type: 'number'
    },
    BuyerFee: {
      type: 'number'
    },
    SellerFee: {
      type: 'number'
    },
  }

};

