/**
 * ArticlesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const  {graphql} = require('graphql');
const schema = require('../graphQL/schema');
const rootValue = require('../graphQL/resolver');
module.exports = {
  // default index action
  index(req, res) { 
    console.log(req.body)
    graphql(
      schema,                       // generated schema
      req.body.query,               // graphql query string
      rootValue,                    // default rootValue
      {                             // context
        request: sails.request,     // default request method - required
        reqData: {                  // object of any data you want to forward to server's internal request
          headers: {/*your headers to forward */}
        }
      }
    ).then((result) => {
      console.log('result :',result)
      res.json(result.data);
    }).catch(err => {
      console.log(err)
    });
  }
};