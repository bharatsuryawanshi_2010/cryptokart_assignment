/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    createUser: async (req, res) => {

        var name = req.body;
    
        
        console.log()
          await Trade.create(name);
          let newTrade =  await Trade.find().sort('TradeID DESC');
                console.log('newData',newTrade[0])
            //   if (err) {
    
            //     return res.serverError(ERROR);
            //   }
    
              if (newTrade) {
    
                /**
                 * Notify connected socket clients about new record creation on Trade model
                 */
                Trade.publishCreate(newTrade);
                return res.send({ status: 'SUCCESS', data: newTrade });
              }
      },
/**
   *  Get users
   * 
   */
  getUsers: function (req, res) {

    Trade.find().exec(function (err, users) {

      if (err) {

        return res.serverError(ERROR);
      }
      return res.send({ status: 'SUCCESS', data: users });
    });
  },

  /**
   * subscribe real time model events of Trade to connect socket client
   * 
   * @param req :: socket request
   *
   */
  subscribe: function (req, res) {
console.log('in subscrib')
    if (!req.isSocket) {

      return res.badRequest({ status: 'NOT_SOCKET_REQUEST' });
    }
    Trade.find().exec(function (err, users) {

      if (err) {

        return res.serverError({ status: 'SERVER_ERROR' });
      }
      /**
       * subscribe the Trade model to connected client sockets
       */
      Trade.subscribe(req, _.pluck(users, 'id'));

      /**
       * Needed for receiving broadcasts every time publishCreate() is called on Trade model
       */
      Trade.watch(req);
      return res.ok({ status: 'SUBSCRIBED' });
    });
  }
};

