
const  {buildSchema} = require('graphql');

module.exports = buildSchema(`
      type commission {
        TotalCommission: Float!
      }
      type Trade {
        TradeID: ID!,
        Timestamp: Int!,
        Market: String!,
        Amount: Float!,
        Unit: String!,
        Price: Float!,
        Currency: String!,
        Total: Float!,
        BuyOrderID: Int!,
        SellOrderID: Int!,
        BuyerFee: Float!,
        SellerFee: Float!,
        commission: commission!
      }

      type Dashboard {
        Unit: String,
        Currency: String,
        TotalBuyerCommission: Float,
        TotalSellerCommission: Float
      }

      type RootQuery {
        tradeDetails: [Trade!]!,
        getCommission(Currency: String!): commission!,
        getDashboardData: [Dashboard]
      }

      input DataInput {
        Market: String!,
        Amount: Float!,
        Unit: String!,
        Price: Float!,
        Currency: String!,
        Total: Float!,
        BuyOrderID: Int!,
        SellOrderID: Int!,
        BuyerFee: Float!,
        SellerFee: Float!
      }

      type RootMutation {
        addData(entry: DataInput): Trade!
      }
      schema {
          query: RootQuery,
          mutation: RootMutation

      }
  `)