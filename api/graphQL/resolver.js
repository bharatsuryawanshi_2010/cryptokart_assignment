module.exports = { 
    // Fetching row from trade table having limit of 10
    tradeDetails: async () => {
      const data = await Trade.find().limit(10);
      /**
      * subscribe the Trade model to connected client sockets
      */
      // Trade.subscribe(req, _.pluck(data, 'TradeID'));
    
      // Trade.watch(req);
      return data;
    },
    getDashboardData: async () => {
      try {
        /**
         * Grouping the currencies and Units 
         * and calculating sum of buyerFeea and SellerFee
         */
        const UnitWiseCommission =  await sails.sendNativeQuery('SELECT Unit, SUM(BuyerFee) as TotalBuyerCommission FROM trade GROUP BY Unit' )
        const CurrencyWiseCommission =  await sails.sendNativeQuery('SELECT Currency, SUM(SellerFee) as TotalSellerCommission FROM trade GROUP BY Currency' )
        
        const UnitWiseCommissionRows = JSON.parse(JSON.stringify(UnitWiseCommission.rows));
        const CurrencyWiseCommissionRows = JSON.parse(JSON.stringify(CurrencyWiseCommission.rows));

        let Dashboard = [];
        
        // Used Map function to merge the data into single array
        // Used two map functions as count of Unit and Currency may be different
        UnitWiseCommissionRows.map( ele => {
          Dashboard.push(ele);
        });
        CurrencyWiseCommissionRows.map( ele => {
          Dashboard.push(ele);
        });
        console.log('DashBoard: ',Dashboard)
        return Dashboard
        } catch (error) {
          console.log(error);
          return error;
        }
    },
    getCommission: async (args)=>{
      try {
        /***
         * calulating sum of BuyerFee and SellerFee
         * and add it together
         * so that we will get the Total commission earned for selected currency
        */ 
        let BuyerFee = await Trade.sum('BuyerFee')
        .where({
          'Unit' : args.Currency
        });
        let SellerFee = await Trade.sum('SellerFee')
        .where({
          'Currency' : args.Currency
        });
        const total =  BuyerFee + SellerFee
        return {TotalCommission: total}  
      } catch (error) {
        console.log(error);
        return error;
      }
    },
    addData: async (args) => {
      console.log(args)
      try {
        let createdTrade = await Trade.create(args.entry);
        Trade.publishCreate(createdTrade);
        return createdTrade  
      } catch (error) {
        return error
      }
    }
  }