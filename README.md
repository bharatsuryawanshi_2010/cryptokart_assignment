# CryptoKart Assignment

Create an application which does basic accounting on commissions we charge our customers & checks for reconciliation. We need you to develop an application which gives synopsis of how much per currency we got commissions. 

### Run Project

Need to install following NPM packages

npm install sails -g

npm install sails-graphql --save

npm install sails-mysql

npm install nodemon -g

### Import Database(mySQL)

Import database by using one on the two file named  - cryptokartCSV.csv and  cryptokartSQL.sql

### To Test the Graph API

Google chrome extension - Altair GraphQL Client

Some Test Commands : 

To check the commission of any Currency or Unit
{
  getCommission(Currency:"BTC"){
    TotalCommission
  }
}

DashBoard - will list commission for all Currencies

{
  getDashboardData {
    	Unit
    Currency
    TotalBuyerCommission
    TotalSellerCommission
  }
}

Mutation Query - to add Trade into database

 mutation{
  addData(
    Market: "ETC/BTC",
        Amount: 0.12,
        Unit: "ETC",
        Price: 3251,
        Currency: "BTC",
        Total: 2121,
        BuyOrderID: 1,
        SellOrderID: 1,
        BuyerFee: 0.012,
        SellerFee: 251,
)
}

### Import Database file

I have pushed database file required for this project in the root directory 

file name : SQL file - cryptokartSQL
            CSV file - cryptokartCSV

Use any of the two to import the database

To run the project

nodemon app.js

### TO Run ANgular

cd SocketClient
npm install
ng serve

